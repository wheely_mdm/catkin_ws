#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above
#    copyright notice, this list of conditions and the following
#    disclaimer in the documentation and/or other materials provided
#    with the distribution.
#  * Neither the name of Willow Garage, Inc. nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# Revision $Id$

## Simple talker demo that listens to std_msgs/Strings published 
## to the 'chatter' topic
import rospy
#import rospy
from sensor_msgs.msg import Image as msgImage

from PIL import Image
import Adafruit_ILI9341 as TFT
import Adafruit_GPIO as GPIO
import Adafruit_GPIO.SPI as SPI

from Queue import Queue
from threading import Thread

# Raspberry Pi configuration.
DC = 18
RST = 17
SPI_PORT = 0
SPI_DEVICE = 0




class Display:
    def __init__(self):
        rospy.init_node('rosdisplay_cam', anonymous=True)
        self.rate = rospy.Rate(10)
        self.imgQ = Queue()
        self.running = True
        rospy.Subscriber('/rosberrypi_cam/image_raw', msgImage, self.callback,buff_size=300000,tcp_nodelay=True)
        # Create TFT LCD display class.
        self.disp = TFT.ILI9341(DC, rst=RST, spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE, max_speed_hz=64000000))
        # Initialize display.
        self.disp.begin()

    def callback(self,data):
        self.imgQ.put(data)
        rospy.loginfo(rospy.get_caller_id() + 'I heard %d,%d', data.height,data.width)


    def dispFcn(self):
        while self.running:            
            rospy.loginfo(rospy.get_caller_id() + 'Queue size %d', self.imgQ.qsize())
            data = None
            while not self.imgQ.empty():
                data = self.imgQ.get()
            if data is None:
                continue
            try:
                image = Image.fromstring("RGB",[data.width,data.height],data.data)
                image.save("test.png")
                # Resize the image and rotate it so it's 240x320 pixels.
                image = image.rotate(90)
                self.disp.display(image)
            except CvBridgeError as e:
                print(e)            
            self.rate.sleep()

    def run(self):
        thread = Thread(target=self.dispFcn)
        thread.start()
        rospy.spin()
        self.running = False
        thread.join()


if __name__ == '__main__':
    listener=Display()
    listener.run()
