#!/usr/bin/env python
# Simple uart terminal parsing wheely protocol both ways
# requires pySerial and RPi.GPIO to run
#
# input of message: 1 digit address followed by any no of data byte in hex
# user input is then encoded and send on UART
# 
# -message frame is started by 0x7E
# -first byte is (address << 4) | (size & 0xF)
# -bytes 0x7E and 0x7D in any position are replaced by 0x7D,byte^0x20
# -escape character 0x7D is not counted in length
# 
# Info for wheelyPi servos:
#   0x31=  0deg
#   0x68= 90deg
#   0x86=120deg

import serial
from Queue import Queue
from threading import Thread, Lock
import signal
import sys
import time

try:
    import RPi.GPIO as GPIO
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(17, GPIO.OUT, initial=GPIO.HIGH)
except:
    sys.stderr.write("Could not find RPi.GPIO, not switching UART converter\n")
    
class whDecoder:
    def __init__(self, outQ):
        self.buf = b""
        self.msg = []
        self.oldmsg=set([])
        self.length= -1
        self.expected = -1
        self.escape =  False
        self.parsing = True
        self.timer = 0
        
        
    def parse(self, rx):
        self.buf = self.buf + rx
        while True:
            if len(self.buf)>0:
                byte=ord(self.buf[0])
                self.buf = self.buf[1:]
                if byte == 0x7E:
                    self.expected = 1
                    self.length = -1
                    self.timer = time.time()
                elif byte == 0x7D:
                    self.escape = True
                else:
                    if self.escape:
                        byte = 0x20^byte
                        self.escape = False
                        
                    if self.length == -1 :
                        self.expected = int(byte&0xF)
                        self.msg = [byte & 0xF0]
                        self.length = 0
                        
                    elif self.expected > 0:
                        self.msg = self.msg + [byte]
                        self.length = self.length + 1
                        self.expected = self.expected -1
                if self.expected == 0:
                    if self.oldmsg != set(self.msg):
                        self.expected = -1
                        self.oldmsg = set(self.msg)
                        return self.msg + [time.time() - self.timer]
            else:
                return None
         
    
class whEncoder:
    def __init__(self):
        self.msg = b''
        self.addr = 0x10
        
    def setAddress (self, address):
        self.addr = address
        
    def add(self, data):
        if isinstance(data, (int, long)):
            data = [data]
        self.msg = self.msg + data
    
    def sendStr(self, dataLst=None, address=None):
        if dataLst is None:
            dataLst = self.msg
        if address is None:
            address = self.addr
        
        strSend = b'\x7E'
        dataLst = [(address&0xF0)|(len(dataLst)&0xF)]+ dataLst
        for i in xrange(0,len(dataLst)):
            byte = dataLst[i]
            if byte == 0x7E or byte == 0x7D:
                strSend = strSend + b"\x7D" + chr(byte^0x20)
            else:
                strSend = strSend + chr(byte) 
                
        return strSend
            
        
class whTerm(object):
    '''
    Minimal terminal for wheely platform UART protocol
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.com = None
        self.msgQ = Queue()
        self.encode = whEncoder()
        self.decode = whDecoder(self.msgQ)
        self.comLock = Lock()
        
        self.rxThread = Thread(target=self.loopRx)
        self.cmdThread= Thread(target=self.loopConsole)
        self.running = False
        
    def start(self):
        self.com = serial.Serial("/dev/ttyAMA0",
                                 baudrate=57600, timeout=0.025)
        self.running = True
        self.rxThread.start()
        self.cmdThread.start()
        
    def loopRx(self):
        msg = b""
        self.com.flushInput()
        while self.running:
            waitBytes = self.com.inWaiting()
            if waitBytes:
                msg = self.decode.parse(self.com.read(waitBytes))
            while msg:                
                print("%s" % msg)
                msg = self.decode.parse(b"")
                    
        
    def stop(self):
        self.running = False
        self.rxThread.join()
        self.cmdThread.join()
        self.com.close()
        sys.exit()       
        
    def loopConsole(self):
        while self.running:
                inStr = ''
                inStr = raw_input()
                if len(inStr) > 0:
                    addr= int(inStr[0])<<4
                    data=[]
                    for i in xrange(1, len(inStr),2):
                        data= data + [int(inStr[i:i+2],16)]
                    outStr = self.encode.sendStr(data, addr)
                    print("Sending %s\n" % 
                                     ','.join("%02x" % ord(outStr[i]) 
                                              for i in xrange(0,len(outStr))))
                    self.com.write(outStr)
            
    def main(self):
        signal.signal(signal.SIGINT, self.handleCtrlC)
        self.start()
        while self.running:
            pass
        self.stop
        
    def handleCtrlC(self,signal, frame):
        print("Ctrl+C was pressed .. exiting")
        self.stop()
        sys.exit()
        
if __name__ == "__main__":
    term = whTerm()
    term.main()
        
