# Wheely catkin workspace

ROS packages for Wheely

Some of the packages can only be compiled on the Raspberry (e.g. camera driver)

Raspicam library needs to be installed on the Raspberry [get it here](https://github.com/cedricve/raspicam)

## Initialization

Make sure ROS is installed, whereever you want to compile this.

Initialize a catkin workspace in *./src* :
```
# catkin_init_workspace
```
Run catkin_make in catkin_ws
```
# cd ..
# catkin_make
```
This should produce a couple of errors, but generates the setup.bash file so let's add the project to the ROS path
```
# source devel/setup.bash
```
Now we can run rosdep on a package:
```
# rosdep install rosberrypi_cam
```
Finally run catkin_make again and it should be ready to run.
More info can be found in the [Wiki](https://bitbucket.org/wheely_mdm/design/wiki/Home)

The camera and display nodes can easily be run together using the launch file:
```
# roslaunch rosdisplay_node disp_cam.launch
```


## Credit

This project adapts parts from the Duckietown project:
[http://duckietown.mit.edu/](http://duckietown.mit.edu/)

The rest is (mostly) original work by the authors

(C) 2016 by Morten Beier, Milan Földi, Denis Kirchhübel
